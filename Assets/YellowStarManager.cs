﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YellowStarManager : MonoBehaviour {
    public bool YellowEnabled;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "boy")
        {
            YellowEnabled = true;
        }
    }
}
