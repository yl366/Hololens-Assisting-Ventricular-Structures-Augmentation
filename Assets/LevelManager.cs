﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour {

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    public void StartGame()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Demo");
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void Help()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Help");
    }

    public void Return()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Start");
    }
}
