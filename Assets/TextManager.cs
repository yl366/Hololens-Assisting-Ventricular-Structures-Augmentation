﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextManager : MonoBehaviour {
    public GameObject Stars;
    public GameObject highText;
    public GameObject flyText;
    public GameObject longText;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Stars.GetComponentInChildren<YellowStarManager>().YellowEnabled == true)
        {
            highText.GetComponent<TextMesh>().text = "High Jumping";
        }

        if (Stars.GetComponentInChildren<RedStarManager>().RedEnabled == true)
        {
            flyText.GetComponent<TextMesh>().text = "Flying";
        }

        if (Stars.GetComponentInChildren<BlueStarManager>().BlueEnabled == true)
        {
            longText.GetComponent<TextMesh>().text = "Long Jumping";
        }
    }
}
