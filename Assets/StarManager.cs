﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarManager : MonoBehaviour {
    float t;
	// Use this for initialization
	void Start () {
        gameObject.SetActive(true);
        t= 0.0f;
	}
	
	// Update is called once per frame
	void Update () {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "boy")
        {
            gameObject.SetActive(false);
        }
    }

}
