﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundSound : MonoBehaviour {
    public AudioSource clip;
	// Use this for initialization
	void Start () {
        clip.Play();
	}
	
	// Update is called once per frame
	void Update () {
        clip.Play();
    }
}
