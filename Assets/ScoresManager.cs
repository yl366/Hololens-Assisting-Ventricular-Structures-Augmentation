﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoresManager : MonoBehaviour {
    int score;

    public AudioSource clip1;
    public AudioSource clip2;
    public AudioSource clip3;

    public GameObject Text1;

    public GameObject Backgrounds;

    public bool end;

	// Use this for initialization
	void Start () {
        score = 150;
        Text1.GetComponent<TextMesh>().text = "150";
        end = false;
    }
	
	// Update is called once per frame
	void Update () {

	}

    private void OnTriggerEnter(Collider other)
    {
        if (score >= 200)
        {
            if (other.gameObject.tag == "cube")
            {
                end = true;
                UnityEngine.SceneManagement.SceneManager.LoadScene("Win");
            }

            if (other.gameObject.tag == "tree")
            {
                score -= 30;
                clip1.Play();
                Text1.GetComponent<TextMesh>().text = score.ToString();

            }

            if (other.gameObject.tag == "bird")
            {
                score -= 10;
                clip2.Play();
                Text1.GetComponent<TextMesh>().text = score.ToString();
            }

            if (other.gameObject.tag == "star")
            {
                score += 20;
                clip3.Play();
                Text1.GetComponent<TextMesh>().text = score.ToString();
            }
        }
       

        else
        {
            if (other.gameObject.tag == "cube")
            {
                end = true;
                UnityEngine.SceneManagement.SceneManager.LoadScene("Win");
            }

            if (other.gameObject.tag == "tree")
            {
                score -= 30;
                clip1.Play();
                Text1.GetComponent<TextMesh>().text = score.ToString();

            }

            if (other.gameObject.tag == "bird")
            {
                score -= 10;
                clip2.Play();
                Text1.GetComponent<TextMesh>().text = score.ToString();
            }

            if (other.gameObject.tag == "star")
            {
                score += 20;
                clip3.Play();
                Text1.GetComponent<TextMesh>().text = score.ToString();
            }

        }  
    }

    public void high()
    {
        score -= 30;
    }

    public void fly()
    {
        score -= 50;
    }
}
