﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedStarManager : MonoBehaviour {
    public bool RedEnabled;
	// Use this for initialization
	void Start () {
        RedEnabled = false;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "boy")
        {
            RedEnabled = true;
        }
    }
}
