﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class keyboardController : MonoBehaviour {
    public GameObject boy;
    public GameObject Stars;
    public int High;
    public int Fly;
    public int Long;
    
    // Use this for initialization
    void Start () {
        High = 0;
        Fly = 0;
        Long = 0;
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.UpArrow))
        { 
            boy.GetComponent<Animation>().Play("Normal Jumping");
        }

        if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            boy.GetComponent<Animation>().Stop("Normal Jumping");
            Vector3 pos=boy.transform.position;
            pos.y = -28.94f;
            boy.transform.position = pos;
        }

        if (Input.GetKeyDown(KeyCode.H) )
        {
            boy.GetComponent<Animation>().Play("High Jumping");
            High += 1;
            if (High > 2)
            {
                boy.SendMessage("high");
            }
        }

        if (Input.GetKeyDown(KeyCode.F) )
        {
            boy.GetComponent<Animation>().Play("Flying");
            Fly += 1;
            if (Fly > 2)
            {
                boy.SendMessage("fly");
            }
        }

        if (Input.GetKeyDown(KeyCode.L) )
        {
            boy.GetComponent<Animation>().Play("Long Jumping");
            Long += 1;
            if (Long > 2)
            {
                boy.SendMessage("fly");
            }
        }

        

    }
}
