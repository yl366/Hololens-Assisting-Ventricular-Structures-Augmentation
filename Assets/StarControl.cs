﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarControl : MonoBehaviour {
    public GameObject star1;
    public GameObject star2;
    public GameObject star3;
    public GameObject star4;
    public GameObject star5;
    public GameObject star6;
    public GameObject star7;
    public GameObject star8;
    public GameObject star9;
    public GameObject star10;
    public GameObject star11;
    public GameObject star12;
    public GameObject star13;
    float t;
    bool active;

    // Use this for initialization
    void Start () {
        star1.SetActive(true);
        star3.SetActive(true);
        star5.SetActive(true);
        star7.SetActive(true);
        star9.SetActive(true);
        star11.SetActive(true);
        star13.SetActive(true);

        star2.SetActive(false);
        star4.SetActive(false);
        star6.SetActive(false);
        star8.SetActive(false);
        star10.SetActive(false);
        star12.SetActive(false);

        t = 0.0f;
        active = true;

	}
	
	// Update is called once per frame
	void Update () {
        if (Time.time - t > 0.5f && active == true)
        {
            star1.SetActive(false);
            star3.SetActive(false);
            star5.SetActive(false);
            star7.SetActive(false);
            star9.SetActive(false);
            star11.SetActive(false);
            star13.SetActive(false);

            star2.SetActive(true);
            star4.SetActive(true);
            star6.SetActive(true);
            star8.SetActive(true);
            star10.SetActive(true);
            star12.SetActive(true);

            t = Time.time;
            active = false;
        }

        else if (Time.time - t > 0.5f && active == false)
        {
            star1.SetActive(true);
            star3.SetActive(true);
            star5.SetActive(true);
            star7.SetActive(true);
            star9.SetActive(true);
            star11.SetActive(true);
            star13.SetActive(true);

            star2.SetActive(false);
            star4.SetActive(false);
            star6.SetActive(false);
            star8.SetActive(false);
            star10.SetActive(false);
            star12.SetActive(false);

            t = Time.time;
            active = true;
        }

    }
}
